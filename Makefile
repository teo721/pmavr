all: main.hex

main.elf: main.c usart.c
	avr-gcc -mmcu=atmega324p -DF_CPU=16000000 -std=c99 -Wall -Os -o $@ $^

main.hex: main.elf
	avr-objcopy -j .text -j .data -O ihex main.elf main.hex
	avr-size main.elf

run: main.hex
	sudo bootloadHID -r main.hex

clean:
	rm -rf main.elf main.hex

.PHONY: all clean

