#include <avr/io.h>
#include <stdio.h>
#include <util/delay.h>

#include "usart.h"

#define F_CPU 16000000

void init_motors(){
	DDRB |= (1 << PB0);
	DDRB |= (1 << PB1);
	DDRB |= (1 << PB2);
	DDRB |= (1 << PB3);
}

void go_forward(){
	PORTB |= (1 << PB0);
	PORTB &= ~(1 << PB1);
	
	PORTB |= (1 << PB2);
	PORTB &= ~(1 << PB3);
}

void go_backward(){
	PORTB |= (1 << PB1);
	PORTB &= ~(1 << PB0);

	PORTB |= (1 << PB3);
	PORTB &= ~(1 << PB2);
}


void turn_left(){
	PORTB |= (1 << PB1);
	PORTB &= ~(1 << PB0);

	PORTB &= ~(1 << PB3);
	PORTB &= ~(1 << PB2);

}

void turn_right(){
	PORTB |= (1 << PB3);
	PORTB &= ~(1 << PB2);

	PORTB &= ~(1 << PB1);
	PORTB &= ~(1 << PB0);
}

void stop(){
	PORTB &= ~(1 << PB0);
	PORTB &= ~(1 << PB1);
	PORTB &= ~(1 << PB2);
	PORTB &= ~(1 << PB3);
}

void rotate(){

	PORTB &= ~(1 << PB1);
	PORTB |= (1 << PB0);

	PORTB |= (1 << PB3);
	PORTB &= ~(1 << PB2);


}


int main()
{	
	/* LED - Setam pinul 0 al portului A ca pin de iesire. */
	DDRA |= (1 << PA0);
	init_motors();	
	
	USART0_init();
	
			
	while (1) {
	
		char c = USART0_receive();
			
		if(c == '1') {
		
			PORTA |= (1 << PA0);
			_delay_ms(10);
			go_forward();
			
		}
		else if(c == '0') {
			
			PORTA &= ~(1 << PA0);
			stop();
			_delay_ms(10);
		}
		else if(c == 'f') {
			go_forward();		
		}
		else if(c == 'b') {
			go_backward();
		}
		else if(c == 's') {
			stop();
		}
		else if(c == 'r') {
			turn_right();
		}
		else if(c == 'l') {
			turn_left();
		}
		else if(c == 'o') {
			rotate();
		}
	}
	
    return 0;
}


